class CreateActivities < ActiveRecord::Migration[5.0]
  def change
    create_table :activities do |t|
      t.string :matter
      t.string :task
      t.date :dt_start
      t.date :dt_end
      t.string :concept
      t.integer :semester_id

      t.timestamps
    end
  end
end
