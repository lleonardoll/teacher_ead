# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# STUDENTS
u = Student.new
u.email = 'admin@admin.com'
u.password = '123456'
u.name = 'admin'
u.save

u = Student.new
u.email = 'admin2@admin.com'
u.password = '123456'
u.name = 'admin2'
u.save


# COURSES
c = Course.new
c.name = 'Ánalise e Desenvolvimento de Sistemas'
c.student_id = 1
c.save 

c = Course.new
c.name = 'Marketing'
c.student_id = 1
c.save

c = Course.new
c.name = 'Logistica'
c.student_id = 2
c.save


# SEMESTERS
s = Semester.new
s.name = "1° Semestre"
s.course_id = 1
s.save

s = Semester.new
s.name = "2° Semestre"
s.course_id = 1
s.save

s = Semester.new
s.name = "1° Semestre"
s.course_id = 2
s.save


# ACTIVITYS
a = Activity.new
a.matter = 'IHC'
a.task = 'AV1'
a.dt_start = '02-01-2017'
a.dt_end = '08-02-2017'
a.concept = 'Aprovado'
a.semester_id = 1
a.save

a = Activity.new
a.matter = 'IHC'
a.task = 'AV2'
a.dt_start = '02-01-2017'
a.dt_end = '08-02-2017'
a.concept = ''
a.semester_id = 1
a.save

a = Activity.new
a.matter = 'SI'
a.task = 'PP'
a.dt_start = '02-01-2017'
a.dt_end = '08-02-2017'
a.concept = 'Aprovado'
a.semester_id = 2
a.save

a = Activity.new
a.matter = 'EPS'
a.task = 'PP'
a.dt_start = '02-01-2017'
a.dt_end = '08-02-2017'
a.concept = 'Reprovado'
a.semester_id = 3
a.save