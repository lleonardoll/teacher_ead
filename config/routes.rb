Rails.application.routes.draw do
  
	root "welcome#index"
	devise_for :students

	resources :courses
	resources :semesters
	resources :activitys
end
