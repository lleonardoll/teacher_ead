//= require jquery
//= require js-routes

//= require angular
//= require angular-resource
//= require angular-animate
//= require angular-aria
//= require angular-material
//= require angular-mask
//= require ng-file-upload
//= require jquery_ujs 

//= require api/angular/app
//= require_tree .

function showToast(content){
	$('#toast').addClass('toast');
	$('.mdl-snackbar__text').text(content);
		
	setTimeout(function(){
		$('#toast').removeClass('toast');
		$('.mdl-snackbar__text').text('');
	}, 5000);
};

$(function(){
	$(".deleteDialog").dialog({
		autoOpen: false,
	});
});

function openDeleteDialog(semester){
	if(semester){
		$(".deleteDialog").filter("." + semester).dialog("open");
	}
	else{
		$(".deleteDialog").dialog("open");	
	}
}

function closeDeleteDialog(){
	$(".deleteDialog").dialog("close");
}