(function() {
  'use strict';

  angular
    .module('app')
    .factory('activityAPI', activityAPI);

  activityAPI.$inject = ['$resource'];

  function activityAPI($resource){
    return $resource('', {id: '@id'}, {
      index: {url: '/api/activitys', method: 'GET', isArray: true},
      // new: {url: '/api/users/new', method: 'GET'},
      create: {url: '/api/activitys', method: 'POST'},
      // edit: {url: '/api/users/:id/edit', method: 'GET'},
      update: {url: '/api/activitys/:id', method: 'PATCH'},
      destroy: {url: '/api/activitys/:id', method: 'DELETE'},
      show: {url: '/api/activitys/:id', method: 'GET'}
    });
  }
})();
