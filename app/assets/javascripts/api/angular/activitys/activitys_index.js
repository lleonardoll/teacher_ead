(function(){
  'use strict';

  angular
    .module('app')
    .controller('ActivityIndex', ActivityIndex);

  ActivityIndex.$inject = ['activityAPI'];

  function ActivityIndex(activityAPI){
    var vm = this;
    vm.submit = submit;
    vm.loadActivitys = loadActivitys;
    vm.destroy = destroy;
    vm.selectToDestroy = selectToDestroy;
    vm.order_by = order_by;

    var activityUpdate = {edit: false, activityID: 0 }
    
    function loadActivitys(semesterID){
      var params = { semesterID: semesterID }
      activityAPI.index(params, function(data){
        vm.activitys = data;
      }, function(errors){
      })
    }

    function order_by(column){
      if(column == 'matter'){
        if(vm.order == 'matter'){
          vm.order = '-matter';  
        }
        else{
          vm.order = 'matter';
        }
      }
      else if(column == 'task'){
        if(vm.order == 'task'){
          vm.order = '-task';
        }
        else{
          vm.order = 'task'; 
        }
      }
      else if(column == 'dt_start'){
        if(vm.order == 'dt_start'){
          vm.order = '-dt_start';  
        }
        else{
          vm.order = 'dt_start';
        }
      }
      else if(column == 'dt_end'){
        if(vm.order == 'dt_end'){
          vm.order = '-dt_end';  
        }
        else{
          vm.order = 'dt_end';  
        }
      }
      else if(column == 'concept'){
        if (vm.order == 'concept'){
          vm.order = '-concept';  
        }
        else{
          vm.order = 'concept';  
        }
      }
    }

    function submit(action) {
      if ($.isNumeric(action)){
        activityUpdate.activityID = action;
        update().then(function(data){
          delete vm.activity;
          loadActivitys(data.semester_id);
          showToast('Atividade ATUALIZADA!');
        }, function(errors) {
          vm.errors = errors.data.errors;
        });
      }
      else{
        save().then(function(data){
          delete vm.activity;
          loadActivitys(data.semester_id);
          showToast('Atividade REGISTRADA!');
        }, function(errors) {
          vm.errors = errors.data.errors;
        });
      }
    }

    function save(){
      return activityAPI.create({activity: vm.activity}).$promise;
    }

    function update(){
      return activityAPI.update({id: activityUpdate.activityID, activity: vm.activity}).$promise;
    }

    function destroy(){
      vm.params = {id: vm.activityToDestroy.id};
      var semester_id = vm.activityToDestroy.semester_id;

      activityAPI.destroy(vm.params, function() {
        loadActivitys(semester_id);
        showToast('Atividade APAGADA!');
      }, function(errors) {
        vm.errors = errors.data.errors;
      });
    };

    function selectToDestroy(activity){
      vm.activityToDestroy = activity;
    }

  }
})();