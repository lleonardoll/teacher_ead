(function() {
  'use strict';

  angular
    .module('app')
    .factory('courseAPI', courseAPI);

  courseAPI.$inject = ['$resource'];

  function courseAPI($resource){
    return $resource('', {id: '@id'}, {
      index: {url: '/api/courses', method: 'GET', isArray: true},
      // new: {url: '/api/users/new', method: 'GET'},
      create: {url: '/api/courses', method: 'POST'},
      // edit: {url: '/api/users/:id/edit', method: 'GET'},
      update: {url: '/api/courses/:id', method: 'PATCH'},
      destroy: {url: '/api/courses/:id', method: 'DELETE'},
      show: {url: '/api/courses/:id', method: 'GET'}
    });
  }
})();
