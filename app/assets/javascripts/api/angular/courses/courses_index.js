(function(){
  'use strict';

  angular
    .module('app')
    .controller('CourseIndex', CourseIndex);

  CourseIndex.$inject = ['courseAPI', '$window'];

  function CourseIndex(courseAPI, $window){
    var vm = this;
    vm.destroy = destroy;
    vm.submit = submit;
    vm.editCourse = editCourse;
    vm.getCourse = getCourse;
    vm.loadCourses = loadCourses;

    var courseEditable = {edit: false, course: 0};
    
    function loadCourses(id){
      if (!vm.currentStudent){
        vm.currentStudent = {student: id};
      }
      courseAPI.index(vm.currentStudent, function(data){
        vm.courses = data;
      }, function(errors){
        vm.errors = errors.data.errors;
      });
    };

    function destroy(){
      vm.params = {id: vm.courseDelete.id};
      courseAPI.destroy(vm.params, function() {
        loadCourses();
        showToast('Curso APAGADO!');
      }, function(errors) {
        vm.errors = errors.data.errors;
      });
    };

    function submit() {
      if (courseEditable.edit == false){
        save().then(function(data){
          delete vm.course;
          loadCourses();
          showToast('Curso REGISTRADO!');
        }, function(errors) {
          vm.errors = errors.data.errors;
        });
      }
      else{
        update().then(function(data){
          courseEditable.edit = false;
          showToast('Curso EDITADO!');
          delete vm.course;
          loadCourses();
        }, function(errors) {
          vm.errors = errors.data.errors;
        }); 
      }
    }

    function save(){
      return courseAPI.create({course: vm.course}).$promise;
    }

    function update(){
      return courseAPI.update({id: courseEditable.course, course: vm.course}).$promise;
    }

    function editCourse(course){
      if (course.editable == true){
        courseEditable.edit = false;
        return course.editable = false;
      }
      else{
        courseEditable.edit = true;
        courseEditable.course = course.id;
        return course.editable = true;
      }
    }

    function getCourse(course){
      vm.courseDelete = course;
    }

  }
})();