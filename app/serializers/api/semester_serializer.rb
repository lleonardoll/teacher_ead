class API::SemesterSerializer < ActiveModel::Serializer
	attributes 	:name, :course_id
	belongs_to 	:course
	has_many		:activitys
end