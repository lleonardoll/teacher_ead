class API::CourseSerializer < ActiveModel::Serializer
	attributes 	:id, :name, :student_id
	has_many 		:semesters
end