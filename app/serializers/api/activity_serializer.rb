class API::ActivitySerializer < ActiveModel::Serializer
	attributes :id, :matter, :task, :dt_start, :dt_end, :concept, :semester_id
	belongs_to :semester
end