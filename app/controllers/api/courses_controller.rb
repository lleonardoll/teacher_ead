class API::CoursesController < API::APIController

	def index
		if params[:student]
			render json: Course.where('student_id = ' + params[:student])
		else
			render json: Course.all
		end
	end

	def create
		@course = Course.new(course_params)
		@course.student_id = current_student.id

		if @course.save
			render json: @course
		else
			render json: @course.errors, status: :no_content
		end
	end

	def update
		@course = Course.find params[:id]

		if @course.update_attributes(course_params)
			render json: @course
		else
			render json: @course.errors, status: :no_content
		end
	end

	def destroy
		@course = Course.find params[:id]
		@course.destroy

		render json: {status: :ok}
	end 

	def show
		@semester = Semester.new

		@course = Course.find params[:id]
		render json: @course
	end

	private
		def course_params
			params.require(:course).permit(:name, :student_id)
		end

end