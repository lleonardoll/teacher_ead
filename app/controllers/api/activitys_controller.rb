class API::ActivitysController < API::APIController

	def index
		if params[:semesterID]
			render json: Activity.where('semester_id = ' + params[:semesterID])
		else
			render json: Activity.all
		end
	end

	def create
		@activity = Activity.new(activity_params)

		if @activity.save
			render json: @activity
		else
			render json: @activity, status: :no_content
		end
	end

	def destroy
		@activity = Activity.find params[:id]
		@activity.destroy

		render json: {status: :ok}
	end

	def show
		@activity = Activity.find params[:id]
		render json: @activity, status: :ok
	end

	def update
		@activity = Activity.find params[:id]

		if @activity.update_attributes(activity_params)
			render json: @activity
		else
			render json: @activity, status: :no_content
		end
	end

	private
		def activity_params
			params.require(:activity).permit(
				:matter, :task, 
				:dt_start, :dt_end, 
				:concept, :semester_id);
		end

end