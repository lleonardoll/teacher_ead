class API::SemestersController < API::APIController

	def index
		if params[:courseID]
			render json: Semester.where('course_id = ' + params[:courseID])
		else
			render json: Semester.all
		end
	end

end