module ApplicationHelper

	def nav_content
		if current_student
			render "layouts/nav_with_student"
		else
			render "layouts/nav_without_student"
		end
	end

	def verify_user_in_activity
		if @course.student_id == current_student.id
			render "courses/edit_activity"
		end
	end

end